export class Grid {
    private pacManOrientation: string = "up"
    private pacManX: number = 1
    private pacManY: number = 1
    private rules: Record<string, Rule> = {
        left: {
            symbol: ">",
            moveTo: () => { this.pacManX-- }
        },
        right: {
            symbol: "<",
            moveTo: () => { this.pacManX++ }
        },
        up: {
            symbol: "v",
            moveTo: () => { this.pacManY-- }
        },
        down: {
            symbol: "^",
            moveTo: () => { this.pacManY++ }
        }
    }

    public state: Array<Array<string>> = [
        [".", ".", "."],
        [".", "v", "."],
        [".", ".", "."]
    ]

    public swing(direction: string): void {
        this.pacManOrientation = direction
        this.refreshState()
    }

    public move(): void {
        const pacMan: Rule = this.rules[this.pacManOrientation]
        pacMan.moveTo()
        this.refreshState()
    }

    private refreshState(): void {
        const pacMan: Rule = this.rules[this.pacManOrientation]
        this.state.forEach((row: Array<string>) => { row.fill(".") })
        this.state[this.pacManY][this.pacManX] = pacMan.symbol
    }
}

type Rule = {
    symbol: string
    moveTo: () => void
}