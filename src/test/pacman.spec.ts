import { Grid } from "../main/Grid"

describe('Pac man', () => {
    it('is on the grid', () => {
        const grid = new Grid()

        const state = grid.state

        expect(state).toEqual([
            [".", ".", "."],
            [".", "v", "."],
            [".", ".", "."]
        ])
    })

    it('swings to left', () => {
        const grid = new Grid()

        grid.swing("left")

        expect(grid.state).toEqual([
            [".", ".", "."],
            [".", ">", "."],
            [".", ".", "."]
        ])
    })

    it('swings to right', () => {
        const grid = new Grid()

        grid.swing("right")

        expect(grid.state).toEqual([
            [".", ".", "."],
            [".", "<", "."],
            [".", ".", "."]
        ])
    })

    it('moves to left', () => {
        const grid = new Grid()

        grid.swing("left")
        grid.move()

        expect(grid.state).toEqual([
            [".", ".", "."],
            [">", ".", "."],
            [".", ".", "."]
        ])
    })

    it('moves to corner', () => {
        const grid = new Grid()

        grid.move()
        grid.swing("right")
        grid.move()

        expect(grid.state).toEqual([
            [".", ".", "<"],
            [".", ".", "."],
            [".", ".", "."]
        ])
    })
})