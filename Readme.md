# Normas de la Kata
- Disfrutar
- Usar TDD
- Es mas importante el camino que el destino


# Bibliografia
- [Jest](https://jestjs.io/docs/en/getting-started)

# Execute with docker
## Build
```
docker-compose build
```

## Run
```
docker-compose up
```

# Excecute with NPM

## Install dependencies before run
```
npm install
```

## Run
```
npm test
```

## Run watch
```
npm run test:watch
```

# Otras opciones
- [Java](https://bitbucket.org/belike/setup-katas/src/java-set-up/)